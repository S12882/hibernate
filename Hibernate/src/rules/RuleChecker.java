package rules;

import java.util.ArrayList;
import java.util.List;

import user.*;

public class RuleChecker<TEntity> {
	
	
private List<CheckRule<TEntity>> rules = new ArrayList<CheckRule<TEntity>>();
	
	public void addRule(CheckRule<TEntity> rule){
		rules.add(rule);	
	}
	
	public List<CheckResult> check(TEntity entity){
		List<CheckResult> result = new ArrayList<CheckResult>();
		for(CheckRule<TEntity> rule: rules){
			result.add(rule.CheckResult(entity));
		}
		return result;
	}
}
