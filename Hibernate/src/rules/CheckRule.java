package rules;

public interface CheckRule<TEntity> {

	public CheckResult CheckResult(TEntity entity);
	}

