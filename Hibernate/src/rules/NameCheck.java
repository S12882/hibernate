package rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import user.Person;


public class NameCheck {
	
Pattern notAvaliable = Pattern.compile("1234567890?><{[;'.,/]}':+_)(*&!^%|\\#$");
	
	public CheckResult checkRule(Person person) {
		if(person.getFirstName().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(person.getFirstName() == null)
			return new CheckResult("", RuleResult.Error);
		
		Matcher check = notAvaliable.matcher(person.getFirstName());
		if(check.find()) 
			return new CheckResult("", RuleResult.Error);
		
		return new CheckResult("", RuleResult.Ok);
	}

}
