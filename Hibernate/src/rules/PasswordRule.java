package rules;

import user.User;


public class PasswordRule implements CheckRule<User>{


	public rules.CheckResult CheckResult(User entity) {
		if(entity.getPassword()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getPassword().equals(entity.getLogin()))
			return new CheckResult("", RuleResult.Error);
		if(entity.getPassword().length()<8)
			return new CheckResult("Password too short.", RuleResult.Error);
		
		return new CheckResult("", RuleResult.Ok);
	}


}