package UnitOfWork;


public interface UnitOfWorkRepo {

	public void persistAdd(user.Entity entity);
	public void persistUpdate(user.Entity entity);
	public void persistDelete(user.Entity entity);
}
