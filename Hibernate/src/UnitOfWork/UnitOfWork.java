package UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

import user.Entity;
import user.EntityState;

import java.util.LinkedHashMap;
import java.util.Map;

import org.hibernate.SessionFactory;

public class UnitOfWork implements iUnitOfWork{

private Connection connection;
private SessionFactory session;
	private Map<Entity, UnitOfWorkRepo> entities = new LinkedHashMap<Entity, UnitOfWorkRepo>();
	
	
	
	public UnitOfWork(SessionFactory session) {
		super();
		this.session = session;
		
	}
	@Override
	public void commit() {

		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Modified:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			case UnChanged:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void rollback() {
	entities.clear();	
	}

	public void markAsNew(Entity entity, UnitOfWorkRepo repository) {
		entity.setState(EntityState.New);
		entities.put(entity, repository);	
	}

	public void markAsDirty(Entity entity, UnitOfWorkRepo repository) {
		entity.setState(EntityState.Modified);
		entities.put(entity, repository);	
	}
	
	public void markAsDeleted(Entity entity, UnitOfWorkRepo repository) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
	}

}