package Main;

import java.util.ArrayList; 

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import repozytorium.RepositoryCatalog;
import repozytorium.UserRepository;
import rules.*;
import user.*;
import Cache.*;
import UnitOfWork.*;

public class Main {

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {

			System.out.println("SessionFactory Error" + e);
			throw new ExceptionInInitializerError(e);
		}
	}

	public static void main(String[] args) throws Exception {
		
	
		Configuration configuration = new AnnotationConfiguration().addPackage("models").addAnnotatedClass(User.class);
		configuration.configure("hibernate.cfg.xml");
		SessionFactory session = configuration.buildSessionFactory();
		session.openSession();
		session.getCurrentSession().beginTransaction();

		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setNumber("");
		phoneNumber.getCityPrefix();
		phoneNumber.getTypeId();

		Adress address = new Adress();
		address.setCountryId(0);
		address.setRegionId(0);
		address.setCity("");
		address.setStreet("");
		address.setHouseNumber("");
		address.setLocalNumber("");
		address.setZipCode("");
		address.setTypeId(0);
	   
		Person person = new Person();
		person.setFirstName("");
		person.setPhoneNumbers("+480881723");
		person.setAdress("address");

		User U = new User();
		U.setLogin("Andrii");
		U.setPassword("sql");
		U.setPerson(person);

		UnitOfWork uow = new UnitOfWork(session);
		UserRepository R = new UserRepository(null);
		R.setConnection();
		R.update(U);
		
		PasswordRule rule = new PasswordRule();
		RepositoryCatalog catalog = new RepositoryCatalog(session, uow);
		catalog.getUsers().addRule(rule);

		catalog.getUsers().save(U);
		catalog.commit();

		Update updater = new Update(catalog);
		updater.setLifespan(1);
		updater.update(catalog);
		

		@SuppressWarnings("unchecked")
		List<Entity> usersFromDb = (List<Entity>) Cache.getInstance().getItems().get("Users");

		for (Entity dbUser : usersFromDb)
			System.out.println(dbUser.getId() + " " + ((User) dbUser).getLogin() + " " + ((User) dbUser).getPassword());

		
		updater.run();

		for (Entity dbUser : usersFromDb)
			System.out.println(dbUser.getId() + " " + ((User) dbUser).getLogin() + " " + ((User) dbUser).getPassword());
	}

}