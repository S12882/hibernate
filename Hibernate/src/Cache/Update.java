package Cache;

import repozytorium.*;

public class Update implements Runnable {
	
	private int TimeOfLife;
	
	Cache cache = Cache.getInstance();

	public boolean Run = true;
	private RepositoryCatalog repoCache;
	
	public Update(RepositoryCatalog repoCache){
		this.repoCache=repoCache;
	}
	
	public synchronized void run() {
		
		Run = true;
		System.out.println("Updated is running.");
		
		try {
				for(int i = 1;i<=2;i++)
				{
					Thread.sleep(TimeOfLife*1000);
					update(repoCache);
					System.out.println("Cache was Updated");
				}		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Run=false;
		System.out.println("Updated.");
	}
	
	
	public void update(RepositoryCatalog catalog){
		 cache.clear();
	
	 }
	

	public int getLifespan() {
		return TimeOfLife;
	}

	public void setLifespan(int lifespan) {
		this.TimeOfLife = lifespan;
	}

}