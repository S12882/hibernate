package Cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Cache<TEntity> {
	
	
	private Map <String, ArrayList<TEntity> >items = new HashMap<String, ArrayList<TEntity>>();
	private static Object cache = new Object();
	private static Cache instance;
	
	protected Cache() {}
	   
	public static Cache getInstance() {
		if(instance==null){
			synchronized (cache){
				if(instance==null)instance = new Cache();
		}}
		return instance;
	}
	
	public void clear(){
			getItems().clear();
	}
	
	public void put(String Classname,List<TEntity> list){
		if (!getItems().containsKey(Classname)){
			
			ArrayList<TEntity> objects = new ArrayList<TEntity>();
			getItems().put(Classname, objects);
		}
		
		getItems().get(Classname).addAll(list);
	}
	
	public ArrayList<TEntity> get(String key){
		return new ArrayList<TEntity> (getItems().get(key));
	}

	public Map <String, ArrayList<TEntity> > getItems() {
		return items;
	}

	
}