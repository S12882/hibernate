package user;

import javax.persistence.*;

@javax.persistence.Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)

public abstract class Entity {
	
	  @Id @GeneratedValue @Column(name = "id") private int id;
      @Column (name = "entityState")  @Enumerated(EnumType.ORDINAL) private EntityState state;
   
  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }
}