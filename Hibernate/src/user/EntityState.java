package user;

import javax.persistence.*;

public enum EntityState {
    @Enumerated
    New, Deleted, Unknow, Modified, Unmodified, UnChanged,;
}