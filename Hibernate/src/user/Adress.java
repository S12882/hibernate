package user;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "table_addresse")
public class Adress extends Entity {
    @Column(name ="countryId")
    private int countryId;
    @Column(name ="regionId")
    private int regionId;
    @Column(name ="city")
    private String city;
    @Column(name ="street")
    private String     street;
    @Column(name ="house")
    private String    houseNumber;
    @Column(name ="local")
    private String localNumber;
    @Column(name ="zipCode")
    private String zipCode;
    @Column(name ="typeId")
    private int typeId;

    public int getTypeId() {
        return typeId;
    }
    
    public String getZipCode() {
        return zipCode;
    }
    
    public String getLocalNumber() {
        return localNumber;
    }

    
    public String getHouseNumber() {
        return houseNumber;
    }
    
    public String getStreet() {
        return street;
    }
    
    public int getRegionId() {
        return regionId;
    }
    
    public String getCity() {
        return city;
    }
    
    public int getCountryId() {
        return countryId;
    }
    
    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }


    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}