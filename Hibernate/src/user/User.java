package user;


import javax.persistence.Column;

import javax.persistence.ElementCollection;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.util.List;
import java.util.Collections;

@javax.persistence.Entity

@Table(name = "table_user")
public class User extends Entity {
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    
    @OneToOne
    private Person person;
    
    @OneToMany
    private List<RolesPermissions> rolesPermissionsList;
   
    @OneToMany
    private List<UserRoles> userRoles;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<RolesPermissions> getRolesPermissionsList() {
        return rolesPermissionsList;
    }

    public void setRolesPermissionsList(List<RolesPermissions> rolesPermissionsList) {
        this.rolesPermissionsList = rolesPermissionsList;
    }

    public List<UserRoles> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRoles> userRoles) {
        this.userRoles = userRoles;
    }
}