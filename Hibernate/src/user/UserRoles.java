package user;


import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "table_userRoles")
public class UserRoles extends Entity {
	
    @Column(name = "userId")
    private int userId;
    
    @Column(name = "roleId")
    private int roleId;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}