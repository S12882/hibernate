package repozytorium;

import javax.management.relation.Role;
import user.Person;
import user.User;

public interface RepoCatalog {
	
	public IRepository<User> getUsers();
	public IRepository<Person> getPersons();
	public IRepository<Role> getRoles();
	public void commit();
}


