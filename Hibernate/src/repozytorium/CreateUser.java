package repozytorium;

import java.sql.ResultSet;
import java.sql.SQLException;
import user.*;

public class CreateUser implements CreateEntity<User> {

	@Override
	public User build(ResultSet rs) throws SQLException {
		User result = new User();
		result.setId(rs.getInt("id"));
		result.setLogin(rs.getString("login"));
		result.setPassword(rs.getString("password"));
		return result;
	}
}
