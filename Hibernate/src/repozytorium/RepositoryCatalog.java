package repozytorium;

import java.sql.Connection;

import javax.management.relation.Role;

import org.hibernate.SessionFactory;

import UnitOfWork.*;
import user.*;
import rules.*;

public class RepositoryCatalog implements RepoCatalog {

	private iUnitOfWork uow;
	private SessionFactory session;
	private UnitOfWork Unit;
	private RuleChecker<User> userChecker = new RuleChecker<User>();
	private RuleChecker<Entity> personChecker = new RuleChecker<Entity>();

	public RepositoryCatalog(SessionFactory session, UnitOfWork uow){
		super();
		this.session = session;
		this.uow = uow;
	}
	
 
	public UserRepo getUsers() {
		return new UserRepository(session, new CreateUser(), uow, userChecker);
	}

	public IRepository<Role> getRoles() {
		return null;
	}

	public void commit() {
		Unit.commit();
	}

	@Override
	public IRepository<Person> getPersons() {
	
		return null;
	}

	
}