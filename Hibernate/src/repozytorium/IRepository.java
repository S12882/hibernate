package repozytorium;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<TEntity> {
    public void add(TEntity entity);
    public void remove(TEntity entity);
    public void update(TEntity entity);
    public TEntity get(int id);
    public List<TEntity> getAll();
    public void SaveChanges() throws SQLException;
}