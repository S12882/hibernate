package repozytorium;

import javax.management.relation.Role;

import rules.PasswordRule;
import user.Person;
import user.User;

public interface UserRepo extends IRepository<User> {

	public User withRoleName(Role name);
	public User withRoleId(int id); 
	public void addRule(PasswordRule rule);
	public void save(User user);
	
}
