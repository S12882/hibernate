package repozytorium;

import java.util.*;
import java.sql.*;
import java.sql.Date;

import UnitOfWork.*;
import rules.PasswordRule;
import rules.RuleChecker;
import user.*;
import rules.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.management.relation.Role;

import org.hibernate.SessionFactory;

public class UserRepository implements UserRepo {
	
	    SessionFactory session;
	    protected String url = "jdbc:mysql://localhost:3306/repozetorium?"
	          + "user=Andrii&password=sql";
	    protected String tableName;
	    protected List<User> entities;
	    protected Map<Dzialanie, String> operations;   
	private Connection connection;
	private ResultSet resultSet = null;
	private Statement statement = null;

	
	 public UserRepository(String url) throws SQLException {
	        this.url = url;
	        this.connection.setAutoCommit(false);
	        this.connection.setCatalog("Database");
	    }
		
	public UserRepository(SessionFactory session2, CreateUser createUser, iUnitOfWork uow, RuleChecker<User> userChecker) {
		
	}

	private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (session != null) {
	    	  session.close();
	      }
	    } catch (Exception e) {

	    }
	}
	public void setConnection() throws SQLException, ClassNotFoundException{
	 Class.forName("com.mysql.jdbc.Driver");
	  connection = DriverManager
	      .getConnection(url);
	}

	public void add(User user) {
        String str = "insert into " + this.tableName + " values (" +
                user.getPerson().getFirstName() +", " +
                user.getPerson().getsurName() +", " +
                user.getLogin() + ", " +
                user.getPassword() + ", " +
                user.getUserRoles() + ", " +
                user.getRolesPermissionsList() + ", " +
                ");";

        this.operations.put(Dzialanie.Add, str);
        this.entities.add(user);
    }


	private void writeResultSet(ResultSet resultSet) throws SQLException {
		
		while (resultSet.next()) {
		
		  String Name = resultSet.getString("FirstName");
		  String Surname = resultSet.getString("SurName");
		  String Id = resultSet.getString("ID");
		  String DateOfBirth = resultSet.getString("dateOfBirth");

		  System.out.println("Name: " + Name);
		  System.out.println("Surname: " + Surname);
		  System.out.println("Pesel: " + Id);
		  System.out.println("Date Of Bitrh: " + DateOfBirth);

		  }
	 }

	
	public void delete(User user) {
        String str = "remove from " + this.tableName + "where id = " + user.getId() + ";";
        this.operations.put(Dzialanie.Delete, str);
        this.entities.add(user);
    }


	public void remove(User user) {
		 String str = "DROP USER '"  + user.getLogin() +"'@'" + user.getPassword() + "';";
	        this.operations.put(Dzialanie.Delete, str);
	        this.entities.add(user);
	
	}


	  public void update(User user) {
        String str = "update " + this.tableName + " values (" +
                user.getPerson().getFirstName() +", " +
                user.getPerson().getsurName() +", " +
                user.getLogin() + ", " +
                user.getPassword() + ", " +
                user.getUserRoles() + ", " +
                user.getRolesPermissionsList() + ", " +  ") where id = " + user.getId() +";";

        this.operations.put(Dzialanie.Update, str);
        this.entities.add(user);
    }


	
	public List<User> getAll() {
	
		return null;
	}


	public void SaveChanges() throws SQLException {
        for(Dzialanie dzialanie : this.operations.keySet()) {
            this.connection.prepareCall(this.operations.get(dzialanie));
        }
	}


	
	public void addRule(PasswordRule rule) {
			
	}


	public void save(User user) {
		
		
	}

	
	public User withRoleName(Role name) {
		
		return null;
	}

	
	public User withRoleId(int id) {
		
		return null;
	}

	
	public User get(int id) {
		
		return null;
	}

	
}

