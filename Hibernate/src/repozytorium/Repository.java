package repozytorium;



import org.hibernate.SessionFactory;
import UnitOfWork.*;
import rules.*;
import user.*;


public abstract class Repository<TEntity extends Entity> implements IRepository<TEntity>, UnitOfWorkRepo {
	
	protected RuleChecker<Entity> checker;
	protected iUnitOfWork uow;
	protected SessionFactory session;
	protected CreateEntity<TEntity> builder;

public Repository(SessionFactory session, CreateEntity<TEntity> builder, iUnitOfWork uow,
			RuleChecker<Entity> checker) {
		this.uow = uow;
		this.builder = builder;
		this.session = session;
		this.checker = checker;
	}
	
	
	private CheckResult checkRules(TEntity entity) {

		for (CheckResult c : checker.check(entity)) {
			if (c.getResult() == RuleResult.Error) {
				return c;
			}
		}
		return null;
	}

	public void save(TEntity entity) throws Exception {
		CheckResult result = checkRules(entity);

		if (result == null)
			uow.markAsNew(entity, this);
		else
			throw new Exception(result.getMessage());
	}

	public void update(TEntity entity) {
		CheckResult result = checkRules(entity);

		if (result == null)
			uow.markAsDirty(entity, this);
		else
			try {
				throw new Exception(result.getMessage());
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	}


	public void persistAdd(Entity entity) {

		session.getCurrentSession().save(entity);
	}

	public void persistUpdate(Entity entity) {

		session.getCurrentSession().update(entity);
	}

	public void persistDelete(Entity entity) {

		session.getCurrentSession().delete(entity);
	}

}